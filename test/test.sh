#!/bin/bash 
kma_index -i /database/pseudomonas/pseudomonas.fna -o /database/pseudomonas/pseudomonas.ATG -Sparse ATG &> /database/pseudomonas/pseudomonas.log
 
kmerfinder.py -i /test/test.fastq.gz -o /results -db /database/pseudomonas/pseudomonas.ATG -tax /taxonomy/bacteria.name -x

file=/results/results.txt
DIFF=$(diff $file /test/test_results.txt)
if [ "$DIFF" == "" ] && [ -s $file ] ;
   then     
   echo "TEST SUCCEEDED"; 
else
   echo "TEST FAILED";
fi
